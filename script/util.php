<?php
const DOWNLOAD_URL = "https://factorio.com/get-download/stable/headless/linux64";
const DOWNLOAD_NAME = "factorio_headless.tar.xz";
const FACTORIO_DIR = "factorio";

function clean_dir()
{
    exec("rm -rf " . FACTORIO_DIR);
}

function download_server_tar()
{
    if (file_put_contents(DOWNLOAD_NAME, file_get_contents(DOWNLOAD_URL))) {
        echo "File downloaded successfully. \n";
    } else {
        echo "File downloading failed. \n";
        die(1);
    }

}

function unzip_server_tar()
{
    exec('tar -xf ' . DOWNLOAD_NAME);
    unlink(DOWNLOAD_NAME);
}

function configure_server()
{
    $path_configuration = "config-path=__PATH__executable__/../../../config\nuse-system-read-write-data-directories=false";

    if (file_put_contents("factorio/config-path.cfg", $path_configuration)) {
        echo "Path configuration set successfully. \n";
    } else {
        echo "Path configuration failed. \n";
        die(1);
    }

    if (!file_exists("config")) {
        mkdir("config", 0644, true);
    }

    if (!file_exists("config/server-settings.json")) {
        file_put_contents("config/server-settings.json", file_get_contents("factorio/data/server-settings.example.json"));
    }
    if (!file_exists("config/config.ini")) {
        file_put_contents("config/config.ini", file_get_contents("script/config.ini"));
    }

}

function create_world()
{
    if (!file_exists("saves")) {
        mkdir("saves", 0644, true);
    }
    exec('./factorio/bin/x64/factorio --create ./saves/world.zip');
}
